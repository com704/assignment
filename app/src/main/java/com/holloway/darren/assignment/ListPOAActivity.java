package com.holloway.darren.assignment;
// example taken from https://www.mkyong.com/android/android-listview-example/
// left as the example test in th beginning so that I can develop this later but have the working parts connected and in place.
import android.app.ListActivity;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ListPOAActivity extends ListActivity {
    boolean towns, cities, pubs, restaurants, hills, memorials, sports, fountains, libraries, universities, legals, gardens;
    private String northing, easting;
    private ArrayList<Type> types;
    private double getDisance(double lat2, double lon2){
        double lat1 = Double.parseDouble(northing);
        double lon1 = Double.parseDouble(easting);
        double Rad = 6372.8;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double distance = (Rad * c) / 1000;
        DecimalFormat df = new DecimalFormat("#.##");
        return Double.valueOf(df.format(distance));
    }
    public void setupTypes(){
        types.clear();
        types.add( new Type("town", "Towns", towns));
        types.add( new Type("pub", "Pubs", pubs));
        types.add( new Type("city", "Cities", cities));
        types.add( new Type("restaurant", "Restaurants", restaurants));
        types.add( new Type("hill", "Hills", hills));
        types.add( new Type("memorial", "Memorials", memorials));
        types.add( new Type("sport", "Sports", sports));
        types.add( new Type("fountain", "Fountains", fountains));
        types.add( new Type("library", "Libraries", libraries));
        types.add( new Type("university", "University", universities));
        types.add( new Type("garden", "Gardens", gardens));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        towns = prefs.getBoolean("towns", true);
        cities = prefs.getBoolean("cites", true);
        pubs = prefs.getBoolean("pubs", true);
        restaurants = prefs.getBoolean("restaurants", true);
        hills = prefs.getBoolean("hills", true);
        memorials = prefs.getBoolean("memorials", true);
        sports = prefs.getBoolean("sports", true);
        fountains = prefs.getBoolean("fountains", true);
        libraries = prefs.getBoolean("libraries", true);
        universities = prefs.getBoolean("universities", true);
        legals = prefs.getBoolean("legals", true);
        gardens = prefs.getBoolean("gardens", true);
        types = new ArrayList<Type>();
        northing = prefs.getString("north", "0") ;
        easting = prefs.getString("east", "0") ;
        String lat = "";
        String lon = "";
        String distance = "pending";
        SQLHelper sqLite = new SQLHelper(this);
        ArrayList<String> namesArray = new ArrayList<String>();
        Log.d("OpenGL", "N,E: " + northing + ", " + easting);
        if (hills){
            ArrayList<POA> poas = sqLite.SearchType("hill");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                namesArray.add("Hills: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (towns){
            ArrayList<POA> poas = sqLite.SearchType("town");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                //Log.d("OpenGL", "A: " + lat + ", " + lon + ". B: " + poa.getLat() + ", " + poa.getLon());
                namesArray.add("Towns: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (cities){
            ArrayList<POA> poas = sqLite.SearchType("city");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                //Log.d("OpenGL", "A: " + lat + ", " + lon + ". B: " + poa.getLat() + ", " + poa.getLon());

                namesArray.add("City: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (pubs){
            ArrayList<POA> poas = sqLite.SearchType("pub");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                //Log.d("OpenGL", "A: " + lat + ", " + lon + ". B: " + poa.getLat() + ", " + poa.getLon());

                namesArray.add("Pub: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (restaurants){
            ArrayList<POA> poas = sqLite.SearchType("restaurant");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                namesArray.add("Restaurant: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (memorials){
            ArrayList<POA> poas = sqLite.SearchType("memorial");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                namesArray.add("Memorial: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (sports){
            ArrayList<POA> poas = sqLite.SearchType("sport");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                namesArray.add("Sport: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (fountains){
            ArrayList<POA> poas = sqLite.SearchType("fountain");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                namesArray.add("Fountain: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (libraries){
            ArrayList<POA> poas = sqLite.SearchType("library");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                namesArray.add("Library: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (universities){
            ArrayList<POA> poas = sqLite.SearchType("university");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                namesArray.add("University: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (legals){
            ArrayList<POA> poas = sqLite.SearchType("legal");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                namesArray.add("Legal: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        if (gardens){
            ArrayList<POA> poas = sqLite.SearchType("garden");
            for (int i=0; i< poas.size(); i++){
                POA poa = poas.get(i);
                namesArray.add("Garden: " + poa.getName() + ". Distance: " + getDisance(poa.getNorthing(), poa.getEasting()) + "km");
                if (i==poas.size()){

                }
            }
        }
        String[] names = new String[namesArray.size()];
        names = namesArray.toArray(names);
        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_list_poa, names));
        ListView listView = getListView();
        listView.setTextFilterEnabled(true);
        listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), ((TextView) view).getText(), Toast.LENGTH_SHORT).show();
            }
        });
        super.onCreate(savedInstanceState);

    }
}
