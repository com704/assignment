package com.holloway.darren.assignment;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import java.io.IOException;

/**
 * Created by darrenholloway on 21/02/2017.
 */

public class CameraCapturer {
    Camera camera;
    public void openCamera(){
        try {
            camera = Camera.open();
        } catch (Exception e){
            // do something with exception
        }
    }
    public void startPreview(SurfaceTexture surfaceTexture) throws IOException {
        camera.setPreviewTexture(surfaceTexture);
        camera.startPreview();
    }
    public void stopPreview() {
        camera.stopPreview();
        releaseCamera();
    }

    public void releaseCamera() {
        if(camera!=null) {
            camera.release();
            camera=null;
        }
    }
    boolean isActive()
    {
        return camera!=null;
    }
}
