package com.holloway.darren.assignment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by darrenholloway on 14/04/2017.
 */

public class SQLHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "poaapp";
    public SQLHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL ("CREATE TABLE IF NOT EXISTS POAs (ID INT PRIMARY KEY, name VARCHAR(255), type VARCHAR(255), country VARCHAR(255), region VARCHAR(255), lon VARCHAR(255), lat VARCHAR(255), description VARCHAR(255), northing VARCHAR(255), easting VARCHAR(255))");
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL ("DROP TABLE IF EXISTS POAs");
        onCreate(db);
    }
    public long Add(POA poa){
        SQLiteDatabase db = getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("INSERT INTO POAs(ID, name, type, country, region, lon, lat, description, northing, easting) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        stmt.bindLong (1, poa.getID());
        stmt.bindString (2, poa.getName());
        stmt.bindString (3, poa.getType());
        stmt.bindString (4, poa.getCountry());
        stmt.bindString (5, poa.getRegion());
        stmt.bindString (6, poa.getStringLon());
        stmt.bindString (7, poa.getStringLat());
        stmt.bindString (8, poa.getDescription());
        stmt.bindString (9, poa.getStringNorthing());
        stmt.bindString (10, poa.getStringEasting());
        return stmt.executeInsert();
    }
    public Boolean FindId(long id){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery ("SELECT * FROM POAs WHERE ID=?", new String[] { String.valueOf(id) } );
            if (cursor.moveToFirst()) {
                while(!cursor.isAfterLast()) {
                    return true;
                }
            }
            cursor.close();
            return false;
        } finally {
            if(cursor != null)
                cursor.close();
        }

    }
    public ArrayList<POA> SearchId(long id){
        ArrayList<POA> poas = new ArrayList<POA>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery ("SELECT * FROM POAs WHERE ID=?", new String[] { String.valueOf(id) } );
        if (cursor.moveToFirst()) {
            while(!cursor.isAfterLast()) {
                POA poa = new POA(cursor.getLong(cursor.getColumnIndex("ID")),
                        cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getString(cursor.getColumnIndex("type")),
                        cursor.getString(cursor.getColumnIndex("country")),
                        cursor.getString(cursor.getColumnIndex("region")),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("lon"))),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("lat"))),
                        cursor.getString(cursor.getColumnIndex("description")),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("northing"))),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("easting"))));
                poas.add(poa);
                Log.d("OpenGL", String.valueOf(poa.getLat()));
                cursor.moveToNext();
            }
        }
        cursor.close();
        return poas;
    }
    public ArrayList<POA> SearchName(String name){
        ArrayList<POA> poas = new ArrayList<POA>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery ("SELECT * FROM POAs WHERE name=?", new String[] { name } );
        if (cursor.moveToFirst()) {
            while(!cursor.isAfterLast()) {
                POA poa = new POA(cursor.getLong(cursor.getColumnIndex("ID")),
                        cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getString(cursor.getColumnIndex("type")),
                        cursor.getString(cursor.getColumnIndex("country")),
                        cursor.getString(cursor.getColumnIndex("region")),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("lon"))),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("lat"))),
                        cursor.getString(cursor.getColumnIndex("description")),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("northing"))),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("easting"))));
                poas.add(poa);
                cursor.moveToNext();
            }
        }
        return poas;
    }
    public ArrayList<POA> SearchType(String type){
        ArrayList<POA> poas = new ArrayList<POA>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery ("SELECT * FROM POAs WHERE type=?", new String[] { type } );
        if (cursor.moveToFirst()) {
            while(!cursor.isAfterLast()) {
                POA poa = new POA(cursor.getLong(cursor.getColumnIndex("ID")),
                        cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getString(cursor.getColumnIndex("type")),
                        cursor.getString(cursor.getColumnIndex("country")),
                        cursor.getString(cursor.getColumnIndex("region")),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("lon"))),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("lat"))),
                        cursor.getString(cursor.getColumnIndex("description")),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("northing"))),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("easting"))));
                poas.add(poa);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return poas;
    }
    public ArrayList<POA> SearchCountry(String country){
        ArrayList<POA> poas = new ArrayList<POA>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery ("SELECT * FROM POAs WHERE Country=?", new String[] { country } );
        if (cursor.moveToFirst()) {
            while(!cursor.isAfterLast()) {
                POA poa = new POA(cursor.getLong(cursor.getColumnIndex("ID")),
                        cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getString(cursor.getColumnIndex("type")),
                        cursor.getString(cursor.getColumnIndex("country")),
                        cursor.getString(cursor.getColumnIndex("region")),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("lon"))),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("lat"))),
                        cursor.getString(cursor.getColumnIndex("description")),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("northing"))),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("easting"))));
                poas.add(poa);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return poas;
    }
    public ArrayList<POA> SearchRegion(long region){
        ArrayList<POA> poas = new ArrayList<POA>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery ("SELECT * FROM POAs WHERE region=?", new String[] { String.valueOf(region) } );
        if (cursor.moveToFirst()) {
            while(!cursor.isAfterLast()) {
                POA poa = new POA(cursor.getLong(cursor.getColumnIndex("ID")),
                        cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getString(cursor.getColumnIndex("type")),
                        cursor.getString(cursor.getColumnIndex("country")),
                        cursor.getString(cursor.getColumnIndex("region")),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("lon"))),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("lat"))),
                        cursor.getString(cursor.getColumnIndex("description")),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("northing"))),
                        Double.parseDouble(cursor.getString(cursor.getColumnIndex("easting"))));
                poas.add(poa);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return poas;
    }
    // long ID, String name, String country, String region, long lon, long lat, String description, long northing, long easting
    public int Update(POA poa){
        SQLiteDatabase db = getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("UPDATE POAs SET name=?, type=?, country=?, region=?, lon=?, lat=?, description=?, northing=?, easting=? WHERE ID=?");
        stmt.bindString (1, poa.getName());
        stmt.bindString (2, poa.getType());
        stmt.bindString (3, poa.getCountry());
        stmt.bindString (4, poa.getRegion());
        stmt.bindString (5, poa.getStringLon());
        stmt.bindString (6, poa.getStringLat());
        stmt.bindString (7, poa.getDescription());
        stmt.bindString (8, poa.getStringNorthing());
        stmt.bindString (9, poa.getStringEasting());
        return stmt.executeUpdateDelete();
    }
    public int Delete(long ID){
        SQLiteDatabase db = getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("DELETE FROM POAs WHERE ID=?");
        stmt.bindLong (1, ID);
        return stmt.executeUpdateDelete();
    }
    public void clearDatabase(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL ("DROP TABLE IF EXISTS POAs");
        db.execSQL ("CREATE TABLE IF NOT EXISTS POAs (ID INT PRIMARY KEY, name VARCHAR(255), type VARCHAR(255), country VARCHAR(255), region VARCHAR(255), lon VARCHAR(255), lat VARCHAR(255), description VARCHAR(255), northing VARCHAR(255), easting VARCHAR(255))");
    }
    public int DeleteType(String type){
        // nick said not to use
        SQLiteDatabase db = getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("DELETE FROM POAs WHERE type=?");
        stmt.bindString (1, type);
        return stmt.executeUpdateDelete();
    }
}
