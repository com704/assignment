package com.holloway.darren.assignment;

/**
 * Created by darrenholloway on 17/05/2017.
 */

public class Type {
    private String name, label;
    private boolean active;

    public Type (String n, String l, boolean a){
        this.name = n;
        this.label = l;
        this.active = a;
    }
    public boolean isActive(){
        return active;
    }
    public String getName(){
        return name;
    }
    public String getLabel(){
        return label;
    }
}
