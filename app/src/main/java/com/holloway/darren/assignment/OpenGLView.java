
package com.holloway.darren.assignment;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.AttributeSet;
import android.util.Log;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class OpenGLView extends GLSurfaceView implements GLSurfaceView.Renderer {
    private GPUInterface gpu, gpuT;
    private FloatBuffer vbuf, vTbuf;
    private ShortBuffer sbuf, sTbuf;
    private float[] modelView, perspective;
    private float[] currentPosition = {0,0,0,0};
    private SurfaceTexture cameraTexture;
    private CameraCapturer cam;
    private ArrayList<POA> shapes = new ArrayList<POA>();
    private boolean isReady;

    public OpenGLView (Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
        setEGLContextClientVersion(2);
        setRenderer(this);
        modelView = new float[16];
        perspective = new float[16];
        Matrix.setIdentityM(modelView, 0);
        Matrix.setIdentityM(perspective, 0);
        isReady = false;
    }
    public boolean readyCheck(){
        return isReady;
    }
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glClearDepthf(1.0f);
        // changes from lesson version to Nicks version in an attempt to resolve camera not showing
        createShapes();
        final int GL_TEXTURE_EXTERNAL_OES = 0x8d65;
        int[] textureId = new int[1];
        GLES20.glGenTextures(1, textureId, 0);
        if(textureId[0] != 0) {
            GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, textureId[0]);
            GLES20.glTexParameteri(GL_TEXTURE_EXTERNAL_OES,GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GL_TEXTURE_EXTERNAL_OES,GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
            cameraTexture = new SurfaceTexture(textureId[0]);
            // Log.d("OpenGL", "textureId=" + textureId[0]);
            final String texVertexShader =
                    "attribute vec4 aVertex;\n" +
                            "varying vec2 vTextureValue;\n" +
                            "void main (void)\n" +
                            "{\n" +
                            "gl_Position = aVertex;\n" +
                            "vTextureValue = vec2(0.5*(1.0 + aVertex.x), 0.5*(1.0 - aVertex.y));\n" +
                            "}\n",
                    texFragmentShader =
                            "#extension GL_OES_EGL_image_external: require\n" +
                                    "precision mediump float;\n" +
                                    "varying vec2 vTextureValue;\n" +
                                    "uniform samplerExternalOES uTexture;\n" +
                                    "void main(void)\n" +
                                    "{\n" +
                                    "gl_FragColor = texture2D(uTexture,vTextureValue);\n" +
                                    "}\n";
            gpuT = new GPUInterface();
            int vertexR = gpuT.getShader(GLES20.GL_VERTEX_SHADER, texVertexShader);
            int fragmentR = gpuT.getShader(GLES20.GL_FRAGMENT_SHADER, texFragmentShader);
            if (vertexR != -1 && fragmentR != -1){
                gpuT.makeProgram(vertexR, fragmentR);
            } else {
                Log.e("Error", "Unable to compile the texture shader");
            }
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId[0]);
            int refShaderVar = gpuT.getShaderUniformVarRef( "uTexture");
            GLES20.glUniform1i(refShaderVar, 0);
            cam = new CameraCapturer();
            GLES20.glEnable(GLES20.GL_DEPTH_TEST);
            final String vertexShader =
                    "attribute vec3 aVertex;\n" +
                            "uniform mat4 uMvMtx, uPerspMtx;\n" +
                            "void main(void)\n" +
                            "{\n"+
                            "gl_Position = uPerspMtx * uMvMtx * vec4(aVertex, 1.0);\n" +
                            "}\n",
                    fragmentShader =
                            "precision mediump float;\n" +
                                    "uniform vec4 uColour;\n" +
                                    "void main(void)\n"+
                                    "{\n"+
                                    "gl_FragColor = uColour;\n" +
                                    "}\n";
            gpu = new GPUInterface();
            int vertexResult = gpu.getShader(GLES20.GL_VERTEX_SHADER, vertexShader);
            int fragmentResult = gpu.getShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);
            if (vertexResult != -1 && fragmentResult != -1){
                gpu.makeProgram(vertexResult, fragmentResult);
            } else {
                Log.e("Error","Unable to compile shader ");
            }
            float[] vertices = new float[]{0,0,-3, 1,0,-3, 0.5f,1,-3, 0,0,-6, -1,0,-6, -0.5f,1,-6};
            bufferVertices(vertices);
            onResume();
            isReady = true;
        }
    }
    public void bufferVertices(float[] vertices){
        ByteBuffer buf = ByteBuffer.allocateDirect(vertices.length * Float.SIZE);
        buf.order(ByteOrder.nativeOrder());
        vbuf = buf.asFloatBuffer();
        vbuf.put(vertices);
        vbuf.position(0);
    }
    public void onResume(){
       if ( cam!=null ){
           try {
               cam.openCamera();
               cam.startPreview(cameraTexture);
           } catch(IOException e) {
               cam.releaseCamera();
           }
       }
    }
    public void onPause(){
        cam.releaseCamera();
    }
    public void onDrawFrame(GL10 unused) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        float hFov = 40.0f;
        Matrix.setIdentityM(perspective, 0);
        float aspectRatio = (float)getWidth()/(float)getHeight();
        Matrix.perspectiveM(perspective, 0, hFov/aspectRatio, aspectRatio, 0.1f, 100f);
        if (cam.isActive()){
            cameraTexture.updateTexImage();
            gpuT.select();
        }
        gpuT.drawIndexedBufferedData(vTbuf, sTbuf, 12, "aVertex" );
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        gpu.select();
        Matrix.translateM(modelView, 0, -currentPosition[0], -currentPosition[1], -currentPosition[2]);
        Matrix.setIdentityM(modelView,0);
        gpu.sendMatrix("uMvMtx", modelView);
        gpu.sendMatrix("uPerspMtx", perspective);
        Cube cube1 = new Cube(currentPosition[0], currentPosition[1], currentPosition[2]);
        // Cube cube1 = new Cube(0, 0, 0);
        cube1.render(gpu);
        try {
            //if (shapes.size()>0) Log.d("OpenGL", "size of shapes=" + shapes.size());
            for (int i =0; i < shapes.size(); i++){
                POA poa = shapes.get(i);
                gpu.setUniform4fv("uColour", poa.getColour());
                float x = Float.parseFloat(String.valueOf(poa.getEasting()));
                float z = -Float.parseFloat(String.valueOf(poa.getNorthing()));
                float y = 0;
                //if (i<10) Log.d("OpenGL", "Cube x,y,z=" +x+ " " + y + " " +z + ". Current x, y, z =" + currentPosition[0] +  ", " + currentPosition[1] + ", " + currentPosition[2]);
                Cube cube = new Cube( x, y, z );
                cube.render(gpu);
            }
        } catch (NullPointerException e){
            Log.d("OpenGL", "Drawing Null Pointer " + e.getMessage());
        }
    }
    public void setRotationMatrix(float[] rotationMatrix){
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        gpu.select();
        modelView  = rotationMatrix.clone();
    }
    public void setCurrentLocation(float easting, float northing){
        currentPosition[0] = easting;
        currentPosition[1] = 0;
        currentPosition[2] = -northing;
    }
    public void receiveShapes(ArrayList<POA> poas ){
        if(shapes!=null)shapes.clear();
        if (isReady){
            for (int i=0;i<poas.size();i++) {
                shapes.add(poas.get(i));
            }
        }
    }
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        float hFov = 50.0f;
        GLES20.glViewport(0, 0, width, height);
        float aspectRatio = (float)width / height;
        Matrix.setIdentityM(perspective, 0);
        Matrix.perspectiveM(perspective, 0, hFov / aspectRatio, aspectRatio, 0.1f, 100);
    }
    private void createShapes(){
        float[] vertices = new float[]{-1,1,0,  -1,-1,0,   1,-1,0,   1,1,0 };
        short[] indices = {0,1,2,2,3,0};
        ByteBuffer bufI = ByteBuffer.allocateDirect(indices.length * Short.SIZE).order(ByteOrder.nativeOrder());
        sTbuf = bufI.asShortBuffer();
        sTbuf.put(indices);
        sTbuf.position(0);
        ByteBuffer buf = ByteBuffer.allocateDirect(vertices.length * Float.SIZE);
        buf.order(ByteOrder.nativeOrder());
        vTbuf = buf.asFloatBuffer();
        vTbuf.put(vertices);
        vTbuf.position(0);
    }
}
