package com.holloway.darren.assignment;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

public class GPUInterface {
    int vertexShader, fragmentShader, shaderProgram;
    public GPUInterface () {

    }
    public int getShader (int shaderType, String shaderCode){
        int shader = GLES20.glCreateShader(shaderType);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        int[] compileStatus = new int[1];
        GLES20.glGetShaderiv(shader,GLES20.GL_COMPILE_STATUS, compileStatus, 0);
        if(compileStatus[0]==0){
            Log.e("OpenGL", "Error compiling shader: " + GLES20.glGetShaderInfoLog(shader));
            GLES20.glDeleteShader(shader);
            shader = -1;
        }
        return shader;
    }
    public int makeProgram(int vertexShaderId, int fragmentShaderId){
        shaderProgram = GLES20.glCreateProgram();
        vertexShader = vertexShaderId;
        fragmentShader = fragmentShaderId;
        GLES20.glAttachShader(shaderProgram, vertexShaderId);
        GLES20.glAttachShader(shaderProgram, fragmentShaderId);
        GLES20.glLinkProgram(shaderProgram);
        int[] linkStatus = new int[1];
        GLES20.glGetProgramiv(shaderProgram, GLES20.GL_LINK_STATUS, linkStatus, 0);
        if(linkStatus[0]==0){
            Log.e("OpenGL", "Error linking shader program: " + GLES20.glGetProgramInfoLog(shaderProgram));
            GLES20.glDeleteProgram(shaderProgram);
            shaderProgram = -1;
        } else {
            GLES20.glUseProgram(shaderProgram);
            // Log.d("OpenGL", "shaderProgram=" + shaderProgram);
        }
        return shaderProgram;
    }
    private int getShaderAttribVarRef(String shaderVariable){
        return GLES20.glGetAttribLocation(shaderProgram, shaderVariable);
    }
    public int getShaderUniformVarRef(String shaderVariable){
        return GLES20.glGetUniformLocation(shaderProgram, shaderVariable);
    }
    public void setUniform4fv(String UniformVariable, float[] val){
        if(isValid()){
            int ref = GLES20.glGetUniformLocation(shaderProgram,UniformVariable);
            GLES20.glUniform4fv(ref , 1, val, 0 );
        }
    }
    public void sendMatrix(String varName, float[] matrix){
        if (isValid()){
            int ref_uModelViewMatrix = GLES20.glGetUniformLocation(shaderProgram, varName);
            GLES20.glUniformMatrix4fv(ref_uModelViewMatrix, 1, false, matrix, 0);
        }
    }
    public void drawIndexedBufferedData(Buffer vertices, Buffer indices, int stride, String shader){
        if (isValid()){
            //Log.d("OpenGL", "THE SHAPE RENDERING SHADER IS VALID"); // TESTED
            int vertex = getShaderAttribVarRef(shader);
            vertices.position(0);
            indices.position(0);
            GLES20.glEnableVertexAttribArray(vertex);
            GLES20.glVertexAttribPointer(vertex, 3, GLES20.GL_FLOAT, false, stride, vertices);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, indices.limit(), GLES20.GL_UNSIGNED_SHORT, indices);
        }
    }
    public void select(){
        GLES20.glUseProgram(shaderProgram);
    }
    public boolean isValid(){
        // taken from Nicks app
        return shaderProgram >=0;
    }
}
