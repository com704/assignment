package com.holloway.darren.assignment;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by darrenholloway on 13/02/2017.
 */

public class Cube {
    private FloatBuffer vcbuf;
    private ShortBuffer sbuf;

    public Cube( float x, float y, float z  ){

        short[] indices = {0,1,2,2,3,0,   1,5,6,6,2,1,    5,4,7,7,6,5,    4,0,3,3,7,4,    3,2,6,6,7,3,    5,0,4,0,5,1};

        float size = 1f;
        float[] cubeVertices = new float[]{x,y,z,  x+size,y, z,  x+size,y+size,z,  x,y+size,z,  x,y,z-size, x+size,y,z-size, x+size, y+size,z-size,  x,y+size,z-size};
        bufferCubeVertices( cubeVertices, indices );
    }
    private void bufferCubeVertices( float[] cubeVertices, short[] indices ){
        ByteBuffer ibuf = ByteBuffer.allocateDirect(indices.length * Short.SIZE).order(ByteOrder.nativeOrder());
        sbuf = ibuf.asShortBuffer();
        sbuf.put(indices);
        sbuf.position(0);

        ByteBuffer buf = ByteBuffer.allocateDirect( cubeVertices.length * Float.SIZE ).order( ByteOrder.nativeOrder() );
        vcbuf = buf.asFloatBuffer();
        vcbuf.put( cubeVertices );
        vcbuf.position(0);
    }
    public void render( GPUInterface gpu ){
        gpu.drawIndexedBufferedData(vcbuf, sbuf, 12, "aVertex");
    }

}
