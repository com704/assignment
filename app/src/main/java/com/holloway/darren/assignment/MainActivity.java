package com.holloway.darren.assignment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends Activity implements SensorEventListener, LocationListener {
    // Sensor global
    private Sensor accel, mag, compass;
    private float[] accelValues, magValues;
    private float k;
    private ArrayList<POA> active;
    private ArrayList<Type> types;
    private OpenGLView gl;
    private SQLHelper database;
    private String prefLat, prefLon, north, east;
    private double northing, easting;
    // Preferences globals
    private boolean downloads, towns, cites, pubs, restaurants, hills, memorials, sports, fountains, libraries, universities, legals, gardens;
    class FreeMapInterface extends AsyncTask<Void, Void, String> {
        private String url = "http://www.free-map.org.uk/course/msc/ws/poi.php";
        private ArrayList<POA> poas;
        private double north, east, south, west;
        private String n, e, s, w;
        // example of working url : http://www.free-map.org.uk/course/msc/ws/poi.php?bbox=400000,100000,500000,200000
        public FreeMapInterface () {
            north = 0;
            east = 0;
            south = 0;
            west = 0;
        }
        @Override
        protected String doInBackground(Void... unused) {
            HttpURLConnection conn = null;
            double kmSpace = 500;
            south = northing - kmSpace;
            if (south < 0){
                south = 0;
            }
            west = easting - kmSpace;
            if (west < 0){
                west = 0;
            }
            north = northing + kmSpace;
            east = easting + kmSpace;
            n = String.valueOf(Math.round(north));
            e = String.valueOf(Math.round(east));
            s = String.valueOf(Math.round(south));
            w = String.valueOf(Math.round(west));
            try {
                url += "?bbox=" + s + "," + w + "," + n + "," + e ;
                // url = "http://www.free-map.org.uk/course/msc/ws/poi.php?bbox=400000,100000,500000,200000";
                URL fullURL = new URL(url);
                conn = (HttpURLConnection) fullURL.openConnection();
                InputStream in = conn.getInputStream();
                if (conn.getResponseCode() == 200) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String result = "", line;
                    while ((line = br.readLine()) != null)
                        result += line;
                    return result;
                } else {
                    return "HTTP ERROR: " + conn.getResponseCode();
                }
            } catch (IOException e) {
                return e.toString();
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
        }
        public ArrayList<POA> getPOAS(){
            return poas;
        }
        public void onPostExecute(String result) {
            poas = new ArrayList<POA>();
            try {
                JSONArray jsonArray = new JSONArray(result);
                int countingNew = 0;
                for (int i=0; i<jsonArray.length(); i++){
                    JSONObject curObj = jsonArray.getJSONObject(i);
                    String id = curObj.getString("ID"),
                            name = curObj.getString("name"),
                            type = curObj.getString("type"),
                            country = curObj.getString("country"),
                            region = curObj.getString("region"),
                            description = curObj.getString("description");
                    double northing = curObj.getDouble("northing"),
                            lon = curObj.getDouble("lon"),
                            lat = curObj.getDouble("lat"),
                            easting = curObj.getDouble("easting");
                    POA poa = new POA(Long.parseLong(id), name, type, country, region, lon, lat, description, northing , easting);
                    // Log.d("OpenGL", "Check Lat" + poa.getLat());
                    if (!database.FindId(poa.getID())){
                        database.Add(poa);
                        countingNew += 1;
                    }
                    poas.add(poa);
                }
                new AlertDialog.Builder(MainActivity.this).setMessage("Server sent back: " + poas.size() + ". New: " + countingNew + ". North: " + n + ". East: " + e + ". South: " + s + ". West: " + w).setPositiveButton("OK", null).show();
                sendShapes();
            } catch (JSONException e){
                new AlertDialog.Builder(MainActivity.this).setMessage("Could not connect to FreeMap, is your WiFi connected?").setPositiveButton("OK", null).show();
            }
        }
    }
    public void onStart(){
        super.onStart();
        // Sensors
        SensorManager mgr = (SensorManager)this.getSystemService(Context.SENSOR_SERVICE);
        accel = mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        compass = mgr.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mag = mgr.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        try {
            mgr.registerListener(this, accel, SensorManager.SENSOR_DELAY_UI);
            mgr.registerListener(this, mag, SensorManager.SENSOR_DELAY_UI);
        } catch (NullPointerException e){
            Log.d("OpenGL", "Sensors not ready yet");
        }
        k = 0.1f;
        // Location
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        active = new ArrayList<POA>();
        types = new ArrayList<Type>();
        // Set up preferences
        prefLat = prefs.getString("lat", "50.9") ;
        prefLon = prefs.getString("lon", "-1.4") ;
        north = prefs.getString("north", "0");
        east = prefs.getString("east", "0");
        downloads = prefs.getBoolean("autodownload", true);
        towns = prefs.getBoolean("towns", true);
        cites = prefs.getBoolean("cites", true);
        pubs = prefs.getBoolean("pubs", true);
        restaurants = prefs.getBoolean("restaurants", true);
        hills = prefs.getBoolean("hills", true);
        memorials = prefs.getBoolean("memorials", true);
        sports = prefs.getBoolean("sports", true);
        fountains = prefs.getBoolean("fountains", true);
        libraries = prefs.getBoolean("libraries", true);
        universities = prefs.getBoolean("universities", true);
        legals = prefs.getBoolean("legals", true);
        gardens = prefs.getBoolean("gardens", true);
        OSGBProjection proj = new OSGBProjection();
        Point loc = proj.project(new Point(Double.parseDouble(prefLon), Double.parseDouble(prefLat)));
        northing = loc.x;
        easting = loc.y;
        setupTypes();
        webService();
        sendShapes(); // changed to attach colour to the POA and then keep the list ready to go when activity start
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LocationManager loc = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        loc.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        gl = (OpenGLView)findViewById(R.id.gl);
        // Database block
        database = new SQLHelper(this);
    }
    @Override
    protected void onResume(){
        super.onResume();
    }
    @Override
    protected void onPause(){
        SensorManager mgr = (SensorManager)this.getSystemService(Context.SENSOR_SERVICE);
        mgr.unregisterListener(this, accel);
        mgr.unregisterListener(this, mag);
        mgr.unregisterListener(this, compass);
        super.onPause();
    }
    public void onAccuracyChanged(Sensor sensor, int accuracy){
        //ignore
    }
    @Override
    public void onSensorChanged(SensorEvent event){
        // Log.d("OpenGL", "onSensorChanged()");
        accelValues = new float[3];
        magValues = new float[3];
        float[] R = new float[16], I = new float[16], glR = new float[16];

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            for (int i = 0; i<3; i++){
                accelValues[i] = (1-k)*accelValues[i] + k*event.values[i];
            }
        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            for (int i =0; i<3; i++){
                magValues[i] = (1-k)*magValues[i] + k*event.values[i];
            }
        }
        try {
            if (accelValues!=null && magValues!=null){
                SensorManager.getRotationMatrix(R, I, accelValues, magValues);
                WindowManager wm = (WindowManager)this.getSystemService(Context.WINDOW_SERVICE);
                if (wm.getDefaultDisplay().getRotation() == Surface.ROTATION_90){
                    SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, glR);
                    // SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_MINUS_Y, SensorManager.AXIS_X, glR);
                    gl.setRotationMatrix(glR);
                } else if (wm.getDefaultDisplay().getRotation() == Surface.ROTATION_180){
                    Log.d("OpenGL", "Rotation 180");
                    SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_MINUS_Y, glR);
                    gl.setRotationMatrix(glR);
                } else if (wm.getDefaultDisplay().getRotation() == Surface.ROTATION_270){
                    Log.d("OpenGL", "Rotation 270");
                    SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_MINUS_Y, SensorManager.AXIS_X, glR);
                    gl.setRotationMatrix(glR);
                } else {
                    // Rotation is 0;
                    Log.d("OpenGL", "Rotation 0");
                }
            }
        } catch (NullPointerException e){
            Log.d("OpenGL", "Null pointer " + e.getMessage());
        }
    }
    public void setupTypes(){
        types.clear();
        types.add( new Type("town", "Towns", towns));
        types.add( new Type("pub", "Pubs", pubs));
        types.add( new Type("city", "Cities", cites));
        types.add( new Type("restaurant", "Restaurants", restaurants));
        types.add( new Type("hill", "Hills", hills));
        types.add( new Type("memorial", "Memorials", memorials));
        types.add( new Type("sport", "Sports", sports));
        types.add( new Type("fountain", "Fountains", fountains));
        types.add( new Type("library", "Libraries", libraries));
        types.add( new Type("university", "University", universities));
        types.add( new Type("garden", "Gardens", gardens));
    }
    public void sendShapes(){
        active.clear();
        for (int j=0;j<types.size();j++){
            Type t = types.get(j);
            if(t.isActive()){
                ArrayList<POA> poas = database.SearchType(t.getName());
                // Log.d("OpenGL",t.getLabel() +"=" + poas.size());
                for (int i = 0; i < poas.size(); i++){
                    POA poa = poas.get(i);
                    // Log.d("OpenGL", "Check POA: " + poa.toString());
                    poa.setColour(new float[]{1,0,0,1});
                    active.add(poa);
                }
            }
        }
        Log.d("OpenGL","active.size()=" + active.size());
        if(active.size()!= 0){
            Log.d("OpenGL", "Adding local cube");
            POA poa = active.get(1);
            poa.setNorthing(102637.28);
            poa.setEasting(458944.9);
            active.add(poa);
            gl.receiveShapes(active);
        }
    }
    public void webService(){
        if (downloads){
            FreeMapInterface freeMap = new FreeMapInterface();
            freeMap.execute();
        } else {
            new AlertDialog.Builder(MainActivity.this).setMessage("Internet usage has been turned off!").setPositiveButton("OK", null).show();
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.textView) {
            Intent listIntent = new Intent(this, ListPOAActivity.class);
            startActivityForResult(listIntent, 0);
            // show text view screen

            return true;
        } else if (item.getItemId() == R.id.preferences) {
            Intent prefIntent = new Intent(this, Preferences.class);
            startActivityForResult(prefIntent, 0);
            // show preferences screen

            return true;
        } else if (item.getItemId() == R.id.delete){
            database.clearDatabase();
            webService();
            return true;
        }
        return false;
    }
    public void onLocationChanged(Location newLoc) {
        OSGBProjection proj = new OSGBProjection();
        Point loc = proj.project(new Point(newLoc.getLongitude(), newLoc.getLatitude()));
        northing = loc.y;
        easting = loc.x;
        gl.setCurrentLocation((float)loc.x, (float)loc.y);
        // Log.d("OpenGL", "Location Changed New x,y: " + easting + ", " + northing);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("north", String.valueOf(northing));
        editor.putString("east", String.valueOf(easting));
        editor.apply();

    }
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Provider " + provider +
                " disabled", Toast.LENGTH_LONG).show();
    }
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Provider " + provider +
                " enabled", Toast.LENGTH_LONG).show();
    }
    public void onStatusChanged(String provider,int status,Bundle extras) {

        Toast.makeText(this, "Status changed: " + status,
                Toast.LENGTH_LONG).show();
    }
    @Override
    public void onStop(){
        SensorManager mgr = (SensorManager)this.getSystemService(Context.SENSOR_SERVICE);
        mgr.unregisterListener(this, accel);
        mgr.unregisterListener(this, mag);
        super.onStop();
    }
    public void onDestroy(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lat", prefLat);
        editor.putString("lon", prefLon);
        editor.putString("north", String.valueOf(northing));
        editor.putString("east", String.valueOf(easting));
        editor.putBoolean("autodownload", downloads);
        editor.putBoolean("towns", towns);
        editor.putBoolean("cites", cites);
        editor.putBoolean("pubs", pubs);
        editor.putBoolean("restaurants", restaurants);
        editor.putBoolean("hills", hills);
        editor.putBoolean("memorials", memorials);
        editor.putBoolean("sports", sports);
        editor.putBoolean("fountains", fountains);
        editor.putBoolean("libraries", libraries);
        editor.putBoolean("universities", universities);
        editor.putBoolean("legals", legals);
        editor.putBoolean("gardens", gardens);
        editor.commit();
        super.onDestroy();
    }
}
