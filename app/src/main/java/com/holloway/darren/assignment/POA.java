package com.holloway.darren.assignment;

/**
 * Created by darrenholloway on 14/04/2017.
 */

public class POA {
    private long ID;
    private Double lon, lat, northing, easting;
    private String name, type, country, region, description;
    private float[] colour;

    public POA(long ID, String name, String type, String country, String region, double lon, double lat, String description, double northing, double easting){
        this.ID = ID;
        this.name = name;
        this.country = country;
        this.region = region;
        this.type = type;
        this.lon = lon;
        this.lat = lat;
        this.description = description;
        this.northing = northing;
        this.easting = easting;
        this.colour = new float[4];
    }
    public long getID(){
        return this.ID;
    }
    public String getName(){
        return this.name;
    }
    public String getCountry(){
        return this.country;
    }
    public String getRegion(){
        return this.region;
    }
    public String getType(){ return this.type; }
    public double getLon(){ return this.lon; }
    public void setLat(double lat) {this.lat = lat; }
    public String getStringLon(){ return this.lon.toString(); }
    public double getLat(){
        return this.lat;
    }
    public String getStringLat(){ return this.lon.toString(); }
    public String getDescription() {return this.description; }
    public double getNorthing() { return this.northing; }
    public String getStringNorthing() { return this.northing.toString(); }
    public double getEasting() { return this.easting; }
    public String getStringEasting(){ return this.easting.toString(); }
    public void setColour(float[] colour){ this.colour = colour; }
    public float[] getColour(){ return this.colour; }
    public void setNorthing(double n){this.northing = n; }
    public void setEasting(double e){this.easting = e; }
    public String toString(){
        return "ID:" + this.ID + ", Name:" + this.name + ", Country:" + this.country + ", Region:" + this.region
                + ", Type:" + this.type + ", Lon:" + this.lon + ", Lat:" + this.lat + ", Desc" + this.description
                + ", Northing:" + this.northing + ", Easting:" + this.easting + ", Colour:" + this.colour.toString();
    }
}
