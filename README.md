# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This project has been created to complete the unit assignment for Application Development at Southampton Solent University Masters course in AR

### How do I get set up? ###

* This is an android studio project and will require the latest version to run on your local machine

### Contribution guidelines ###

* Most of the content has been the result of a weekly class and provided coding
* Some additional content has been gained from the tutor Nick Whitelegg in an attempt to resolve a coding issue that could not be found

### Who do I talk to? ###

* Owner of the project is Darren Holloway 0holld26@solent.ac.uk